#include <stdio.h>
int main(int argc, char const *argv[]) {
  char arreglo[6];//declaramos un arreglo de tipo char tam=6
  arreglo[0]='A';//vamos ingresando una a una las letras
  arreglo[1]='B';
  arreglo[2]='C';
  arreglo[3]='D';
  arreglo[4]='E';
  arreglo[5]='F';
  //y extraemos sus valores indicando sus subindice
  printf("El valor en el subindice 0 es:%c\n",arreglo[0] );
  printf("El valor en el subindice 1 es:%c\n",arreglo[1] );
  printf("El valor en el subindice 2 es:%c\n",arreglo[2] );
  printf("El valor en el subindice 3 es:%c\n",arreglo[3] );
  printf("El valor en el subindice 4 es:%c\n",arreglo[4] );
  printf("El valor en el subindice 5 es:%c\n",arreglo[5] );

  return 0;
}

